drop database IF EXISTS tobyspring;
create database tobyspring CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
use tobyspring;


create table users
(
    id        varchar(10) primary key,
    name      varchar(20) not null,
    password  varchar(10) not null,
    level     integer,
    loginCnt  integer default 0,
    recommend integer default 0,
    email     varchar (60)
)