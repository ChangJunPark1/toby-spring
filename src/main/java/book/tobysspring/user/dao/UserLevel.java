package book.tobysspring.user.dao;

public enum UserLevel {
    GOLD(3, null), SILVER(2, GOLD), BASIC(1, SILVER);

    private final int value;
    private final UserLevel next;

    UserLevel(int value, UserLevel next){
        this.value = value;
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public UserLevel getNext(){
        return next;
    }

    public static UserLevel valueOf(int value) {
        switch (value) {
            case 1: return BASIC;
            case 2: return SILVER;
            case 3: return GOLD;
            default: throw new AssertionError("Unknown alue : " + value);
        }
    }
}
