package book.tobysspring.user.dao;

import book.tobysspring.user.domain.User;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class JdbcUserDao implements UserDao {
    private final JdbcTemplate jdbcTemplate;

    public JdbcUserDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void add(User user) throws DuplicateKeyException {
        jdbcTemplate.update("insert into users (id, name, password, level, loginCnt, recommend, email) values (?, ?, ?, ?, ?, ?, ?)",
                user.getId(), user.getName(), user.getPassword(), user.getLevel().getValue(), user.getLoginCnt(), user.getRecommend(), user.getEmail());
    }

    public User get(String id) {
        return jdbcTemplate.queryForObject("select * from users where id=?",
                (rs, rowNum) -> new User(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        UserLevel.valueOf(rs.getInt(4)),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getString(7)
                )
                , id);
    }

    public List<User> getAll() {
        return jdbcTemplate.query("select * from users order by id",
                (rs, rowNum) -> new User(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        UserLevel.valueOf(rs.getInt(4)),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getString(7)
                )
        );
    }

    public void deleteAll() {
        jdbcTemplate.update("delete from users");
    }

    public int getCount() {
        Integer result = jdbcTemplate.queryForObject("select count(*) from users", Integer.class);
        if (result == null)
            return 0;
        return result;
    }

    public void update(User user) {
        jdbcTemplate.update("update users set name = ?, password = ?, level = ?, loginCnt = ?, recommend = ?, email = ? where id = ?;",
                user.getName(), user.getPassword(), user.getLevel().getValue(), user.getLoginCnt(), user.getRecommend(), user.getEmail(), user.getId());
    }
}
