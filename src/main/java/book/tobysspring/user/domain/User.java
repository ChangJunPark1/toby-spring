package book.tobysspring.user.domain;

import book.tobysspring.user.dao.UserLevel;

public class User {
    public static final int MIN_LOGIN_CNT_4_SILVER = 50;
    public static final int MIN_RECOMMEND_4_GOLD = 30;

    String id;
    String name;
    String password;
    UserLevel level;
    int loginCnt;
    int recommend;
    String email;

    public User(String id, String name, String password, UserLevel level, int loginCnt, int recommend, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.level = level;
        this.loginCnt = loginCnt;
        this.recommend = recommend;
        this.email = email;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserLevel getLevel() {
        return level;
    }

    public void setLevel(UserLevel level) {
        this.level = level;
    }

    public int getLoginCnt() {
        return loginCnt;
    }

    public void setLoginCnt(int loginCnt) {
        this.loginCnt = loginCnt;
    }

    public int getRecommend() {
        return recommend;
    }

    public void setRecommend(int recommend) {
        this.recommend = recommend;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", level=" + level +
                ", loginCnt=" + loginCnt +
                ", recommend=" + recommend +
                ", email='" + email + '\'' +
                '}';
    }

    public void upgrade(){
        if (level.getNext() == null) {
            System.out.println(this);
            throw new IllegalStateException(level + " is not able to upgrade.");
        }
        else if(isUpgradable())
            level = level.getNext();
    }

    public boolean isUpgradable(){
        switch (level){
            case BASIC: return loginCnt >= MIN_LOGIN_CNT_4_SILVER;
            case SILVER: return recommend >= MIN_RECOMMEND_4_GOLD;
            case GOLD: return false;
            default: throw new IllegalArgumentException("Unknown Level: " + level);
        }
    }


}
