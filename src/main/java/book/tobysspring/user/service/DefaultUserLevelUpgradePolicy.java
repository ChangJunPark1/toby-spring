package book.tobysspring.user.service;

import book.tobysspring.user.domain.User;
import org.springframework.stereotype.Service;

import static book.tobysspring.user.domain.User.MIN_LOGIN_CNT_4_SILVER;
import static book.tobysspring.user.domain.User.MIN_RECOMMEND_4_GOLD;

@Service
public class DefaultUserLevelUpgradePolicy implements UserLevelUpgradePolicy {

    @Override
    public boolean isUpgradable(User user) {
        switch (user.getLevel()) {
            case BASIC:
                return user.getLoginCnt() >= MIN_LOGIN_CNT_4_SILVER;
            case SILVER:
                return user.getRecommend() >= MIN_RECOMMEND_4_GOLD;
            case GOLD:
                return false;
            default:
                throw new IllegalArgumentException("Unknown Level: " + user.getLevel());
        }
    }

    @Override
    public void upgrade(User user) {
        if (user.getLevel().getNext() == null) {
            System.out.println(user);
            throw new IllegalArgumentException(user.getLevel() + " is not able to upgrade.");
        } else {
            user.upgrade();
        }
    }
}
