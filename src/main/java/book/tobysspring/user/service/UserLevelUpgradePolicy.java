package book.tobysspring.user.service;

import book.tobysspring.user.domain.User;

public interface UserLevelUpgradePolicy {
    boolean isUpgradable(User user);
    void upgrade(User user);
}
