package book.tobysspring.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionManager;

@Configuration
public class FactoryBeanConfig {
    final
    PlatformTransactionManager transactionManager;

    final
    UserServiceImpl userServiceImpl;

    public FactoryBeanConfig(PlatformTransactionManager transactionManager, UserServiceImpl userServiceImpl) {
        this.transactionManager = transactionManager;
        this.userServiceImpl = userServiceImpl;
    }

    @Bean
    TxProxyFactoryBean userService(){
        TxProxyFactoryBean userServiceFactory = new TxProxyFactoryBean();
        userServiceFactory.setTarget(userServiceImpl);
        userServiceFactory.setPattern("upgradeAll");
        userServiceFactory.setTransactionManager(transactionManager);
        userServiceFactory.setServiceInterface(UserService.class);
        return userServiceFactory;
    }
}
