package book.tobysspring.user.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {

    private static class DummyMailSender implements MailSender{

        @Override
        public void send(SimpleMailMessage simpleMessage) throws MailException {
            System.out.println(simpleMessage);
        }

        @Override
        public void send(SimpleMailMessage... simpleMessages) throws MailException {

        }
    }

    @Bean
    public MailSender mailSender(){
        return new DummyMailSender();
    }
}
