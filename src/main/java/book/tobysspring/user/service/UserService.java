package book.tobysspring.user.service;

import book.tobysspring.user.domain.User;
import org.springframework.mail.MailSender;

public interface UserService {
    void upgradeAll();
    void add(User user);
    void setMailSender(MailSender mailSender);
}
