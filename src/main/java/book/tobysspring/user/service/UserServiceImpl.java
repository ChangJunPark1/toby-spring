package book.tobysspring.user.service;

import book.tobysspring.user.dao.UserDao;
import book.tobysspring.user.dao.UserLevel;
import book.tobysspring.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;
    private UserLevelUpgradePolicy defaultUserLevelUpgradePolicy;
    // 멀티스레드 환경에서도 안전함 (싱글톤으로 dI 받아도 됨)
    private MailSender mailSender;

    public UserServiceImpl() {
    }

    @Autowired
    public UserServiceImpl(UserDao userDao, UserLevelUpgradePolicy defaultUserLevelUpgradePolicy, MailSender mailSender) {
        this.userDao = userDao;
        this.defaultUserLevelUpgradePolicy = defaultUserLevelUpgradePolicy;
        this.mailSender = mailSender;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setDefaultUserLevelUpgradePolicy(UserLevelUpgradePolicy defaultUserLevelUpgradePolicy) {
        this.defaultUserLevelUpgradePolicy = defaultUserLevelUpgradePolicy;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    protected void upgrade(User user) {
        defaultUserLevelUpgradePolicy.upgrade(user);
        userDao.update(user);
        sendUpgradeEmail(user);
    }

    public void upgradeAll() {
        // 트랜잭션을 가져온다는 것은 트랜잭션을 시작 한다는 것으로 생각
        List<User> users = userDao.getAll();
        users.stream().filter(User::isUpgradable).forEach(this::upgrade);
    }

    public void add(User user) {
        if (user.getLevel() == null) user.setLevel(UserLevel.BASIC);
        userDao.add(user);
    }

    private void sendUpgradeEmail(User user) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setFrom("pcj006@gmail.com");
        mailMessage.setSubject("upgrade");
        mailMessage.setText(user.getName() + ", your level is upgraded.");

        mailSender.send(mailMessage);
    }
}
