package book.tobysspring;

import book.tobysspring.config.JUnitTestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = JUnitTestConfig.class)

public class JUnitTest {
    static ApplicationContext contextObject = null;
    static Set<JUnitTest> testObjects = new HashSet<>();

    @Autowired
    ApplicationContext context;

    @Test
    public void test1() {
        assertThat(testObjects, is(not(hasItem(this))));
        testObjects.add(this);
        assertThat(contextObject == null || contextObject == this.context, is(true));
        contextObject = context;
    }

    @Test
    public void test2() {
        assertThat(testObjects, is(not(hasItem(this))));
        testObjects.add(this);
        assertTrue(contextObject == null || contextObject == this.context);
        contextObject = context;    }

    @Test
    public void test3() {
        assertThat(testObjects, is(not(hasItem(this))));
        testObjects.add(this);
        assertThat(contextObject, either(is(nullValue())).or(is(this.context)));
        contextObject = context;    }
}
