package book.tobysspring.user.dao;

import book.tobysspring.user.domain.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/*
    테스트는 정확하고 빠르게 실행해되어야 함
    UserDao의 오브젝트를 가져와 메소드를 호출 함
    입력 값(User오브젝트)을직접 코드에서 만들어 줌
    테스트 결과를 콘솔에 보여줌

 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoFactory.class)
public class UserDaoTest {

    @Autowired
    private UserDao dao;
    @Autowired
    private DataSource dataSource;

    private User user1;
    private User user2;
    private User user3;

    @Autowired
    private ApplicationContext context;

    @BeforeEach
    public void setUp() {
        dao.deleteAll();

        user1 = new User("1aaa", "ㅁㅁㅁ", "111", UserLevel.BASIC, 1, 0, "pcj006@gmail.com");
        user2 = new User("2bbb", "ㅠㅠㅠ", "222", UserLevel.SILVER, 55, 10, "test@test.test");
        user3 = new User("3ccc", "ㅊㅊㅊ", "333", UserLevel.GOLD, 100, 40, "test@test.test");
    }

    @AfterEach
    public void deleteAllDummy() {
        dao.deleteAll();
        System.out.println("dao.deleteAll();");
    }

    @Test
    public void count() {
        assertThat(dao.getCount(), is(0));

        dao.add(user1);
        assertThat(dao.getCount(), is(1));

        dao.add(user2);
        assertThat(dao.getCount(), is(2));

        dao.add(user3);
        assertThat(dao.getCount(), is(3));
    }

    @Test
    public void addAndGet() {

        User user = new User("pcj006", "창준", "1234", UserLevel.BASIC, 1, 0, "pcj006@gmail.com");

        dao.add(user);
        assertThat(dao.getCount(), is(1));

        System.out.println(user.getId() + " insertion complete :: " + user.getName());

        User userget = dao.get(user.getId());
        checkSameUser(user, userget);

        dao.deleteAll();
        assertThat(dao.getCount(), is(0));

        dao.add(user1);
        dao.add(user2);

        User userget1 = dao.get(user1.getId());
        checkSameUser(userget1, user1);
        User userget2 = dao.get(user2.getId());
        checkSameUser(userget2, user2);

    }

    @Test
    public void getFailureTest() {
        assertThat(dao.getCount(), is(0));

        Exception exception = assertThrows(EmptyResultDataAccessException.class, () -> dao.get("ccc"));

        String expectedMessage = "For input string";
        String actualMessage = exception.getMessage();

        System.out.println(exception.getMessage());
//        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    public void getAllNegative() {
        List<User> users = dao.getAll();
        assertThat(users.size(), is(0));
    }

    @Test
    public void getByWrongId() {
        assertThrows(EmptyResultDataAccessException.class, () -> dao.get("1"));
    }

    @Test
    public void getAll() {
        dao.add(user1);// Id:aaa
        List<User> users1 = dao.getAll();
        assertThat(users1.size(), is(1));
        checkSameUser(user1, users1.get(0));

        dao.add(user2); // Id: bbb
        List<User> users2 = dao.getAll();
        assertThat(users2.size(), is(2));
        checkSameUser(user1, users2.get(0));
        checkSameUser(user2, users2.get(1));

        dao.add(user3);// Id:ccc
        List<User> users3 = dao.getAll();
        assertThat(users3.size(), is(3));
        checkSameUser(user1, users3.get(0));
        checkSameUser(user2, users3.get(1));
        checkSameUser(user3, users3.get(2));
    }

    private void checkSameUser(User user1, User user2) {
        assertThat(user1.getId(), is(user2.getId()));
        assertThat(user1.getName(), is(user2.getName()));
        assertThat(user1.getPassword(), is(user2.getPassword()));
        assertThat(user1.getLevel(), is(user2.getLevel()));
        assertThat(user1.getLoginCnt(), is(user2.getLoginCnt()));
        assertThat(user1.getRecommend(), is(user2.getRecommend()));
        assertThat(user1.getEmail(), is(user2.getEmail()));
    }

    @Test
    public void update() {
        dao.add(user1);
        dao.add(user2);

        user1.setName("박창준");
        user1.setPassword("1234");
        user1.setLevel(UserLevel.GOLD);
        user1.setLoginCnt(1000);
        user1.setRecommend(999);
        dao.update(user1);

        User updated1 = dao.get(user1.getId());
        User updated2 = dao.get(user2.getId());
        checkSameUser(user1, updated1);
        checkSameUser(user2, updated2);
    }
}
