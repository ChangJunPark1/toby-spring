package book.tobysspring.user.service;

import book.tobysspring.user.dao.UserDao;
import book.tobysspring.user.dao.UserLevel;
import book.tobysspring.user.domain.User;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static book.tobysspring.user.domain.User.MIN_LOGIN_CNT_4_SILVER;
import static book.tobysspring.user.domain.User.MIN_RECOMMEND_4_GOLD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    PlatformTransactionManager transactionManager;

    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    @Autowired
    UserLevelUpgradePolicy defaultUserLevelUpgradePolicy;

    @Autowired
    MailSender mailSender;

    List<User> users;

    static class TestUserServiceImpl extends UserServiceImpl {
        private String id;

        public TestUserServiceImpl(String id) {
            super();
            this.id = id;
        }

        @Override
        protected void upgrade(User user) {
            if (user.getId().equals(id)) throw new TestUserServiceException();
            super.upgrade(user);
        }

        private static class TestUserServiceException extends RuntimeException {
        }
    }

    static class MockMailSender implements MailSender {
        private final List<String> requests = new ArrayList<>();

        public List<String> getRequests() {
            return requests;
        }

        @Override
        public void send(@NotNull SimpleMailMessage simpleMessage) throws MailException {
            System.out.println(simpleMessage);
            requests.add(Objects.requireNonNull(simpleMessage.getTo())[0]);
        }

        @Override
        public void send(SimpleMailMessage @NotNull ... simpleMessages) throws MailException {

        }
    }

    @Test
    void beanLoad() {
        assertThat(this.userService, is(notNullValue()));
    }

    @BeforeEach
    public void setUp() {
        userDao.deleteAll();
        users = Arrays.asList(
                new User("1", "박창준", "1234", UserLevel.BASIC, MIN_LOGIN_CNT_4_SILVER - 1, 0, "pcj006@gmail.com"),
                new User("2", "한재승", "1234", UserLevel.BASIC, MIN_LOGIN_CNT_4_SILVER, 0, "pcj002@gmail.com"),
                new User("3", "탁권형", "1234", UserLevel.SILVER, 60, MIN_RECOMMEND_4_GOLD - 1, "pcj003@gmail.com"),
                new User("4", "김근혜", "1234", UserLevel.SILVER, 60, MIN_RECOMMEND_4_GOLD, "pcj004@gmail.com"),
                new User("5", "계승훈", "1234", UserLevel.GOLD, Integer.MAX_VALUE, Integer.MAX_VALUE, "pcj005@gmail.com")
        );
    }

    @Test
    public void upgrade() {
        UserServiceImpl userService = new UserServiceImpl();

        UserDao mockUserDao = mock(UserDao.class);
        when(mockUserDao.getAll()).thenReturn(this.users);
        userService.setUserDao(mockUserDao);

        MailSender mockMailSender = mock(MailSender.class);//new MockMailSender();
        userService.setMailSender(mockMailSender);

        userService.setDefaultUserLevelUpgradePolicy(new DefaultUserLevelUpgradePolicy());

        userService.upgradeAll();

        verify(mockUserDao, times(2)).update(any(User.class));
        verify(mockUserDao).update(users.get(1));
        assertThat(users.get(1).getLevel(), is(UserLevel.SILVER));
        verify(mockUserDao).update(users.get(3));
        assertThat(users.get(3).getLevel(), is(UserLevel.GOLD));

        ArgumentCaptor<SimpleMailMessage> argumentCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        verify(mockMailSender, times(2)).send(argumentCaptor.capture());
        List<SimpleMailMessage> mailMessages = argumentCaptor.getAllValues();
        assertThat(Objects.requireNonNull(mailMessages.get(0).getTo())[0], is(users.get(1).getEmail()));
        assertThat(Objects.requireNonNull(mailMessages.get(1).getTo())[0], is(users.get(3).getEmail()));
    }

    private void checkUserAndLevel(User updated, String expectedId, UserLevel expectedLevel) {
        assertThat(updated.getId(), is(expectedId));
        assertThat(updated.getLevel(), is(expectedLevel));
    }

    private void checkLevelUpgraded(User origin, boolean upgraded) {
        User user = userDao.get(origin.getId());

        if (upgraded) {
            System.out.println("    Upgraded :: " + user);
            assertThat(user.getLevel(), is(origin.getLevel().getNext()));
        } else {
            System.out.println("Not upgraded :: " + user);
            assertThat(user.getLevel(), is(origin.getLevel()));
        }
    }

    @Test
    @DirtiesContext
    public void upgradeAllOrNothing() throws Exception {
        UserServiceImpl testUserServiceImpl = new TestUserServiceImpl(users.get(3).getId());
        MailSender mockMailSender = new MockMailSender();
        testUserServiceImpl.setUserDao(userDao);
        testUserServiceImpl.setDefaultUserLevelUpgradePolicy(defaultUserLevelUpgradePolicy);
        testUserServiceImpl.setMailSender(mockMailSender);

        TxProxyFactoryBean txProxyFactoryBean = applicationContext.getBean("&userService", TxProxyFactoryBean.class);

        txProxyFactoryBean.setTarget(testUserServiceImpl);
        UserService txUserService = (UserService) txProxyFactoryBean.getObject();

        users.forEach(u -> userDao.add(u));

        try {
            Objects.requireNonNull(txUserService).upgradeAll();
            fail("TestUserServiceException expected");
        } catch (TestUserServiceImpl.TestUserServiceException ignored) {
            System.out.println("caught exception");
        }

        for (User user : users) {
            checkLevelUpgraded(user, false);
        }
    }
}
