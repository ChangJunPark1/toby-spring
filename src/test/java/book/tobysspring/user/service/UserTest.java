package book.tobysspring.user.service;

import book.tobysspring.user.dao.UserLevel;
import book.tobysspring.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class UserTest {

    User user;

    @BeforeEach
    public void setUp() {
        user = new User();
    }

    @Test
    public void cannotUpgradeLevel() {
        UserLevel[] levels = UserLevel.values();
        for (UserLevel l : levels) {
            if(l.getNext() != null) continue;
            user.setLevel(l);
            assertThrows(IllegalStateException.class, () -> user.upgrade());
        }
    }
}
