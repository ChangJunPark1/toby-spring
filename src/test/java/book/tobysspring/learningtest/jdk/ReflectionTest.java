package book.tobysspring.learningtest.jdk;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class ReflectionTest {
    @Test
    public void invokeMethod() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String str = "String";

        assertThat(str.length(), is(6));

        Method length = String.class.getMethod("length");
        assertThat(length.invoke(str), is(6));
    }

    @Test
    public void simpleProxyTest(){
        Hello hello = new HelloTarget();

        assertThat(hello.sayHello("a"), is("Hello a"));
        assertThat(hello.sayHi("a"), is("Hi a"));
        assertThat(hello.sayThankYou("a"), is("Thank You a"));

        Hello proxyHello = (Hello) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[]{Hello.class},
                new HelloUppercase(new HelloTarget())
        );

        assertThat(proxyHello.sayHello("a"), is("HELLO A"));
        assertThat(proxyHello.sayHi("a"), is("HI A"));
        assertThat(proxyHello.sayThankYou("a"), is("THANK YOU A"));

    }
}
